package me.deftware.fabric_api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.event.EventBus;
import me.deftware.client.framework.main.Bootstrap;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.utils.OSUtils;
import me.deftware.client.framework.wrappers.IChat;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Main extends EMCMod {

    private static final String dataURL = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/plugins/fabric-api/data.json";
    private Logger logger = LogManager.getLogger("EMC Fabric API Installer");

    @Override
    public void initialize() {
        EventBus.registerClass(this.getClass(), this);
        // 477 = Minecraft 1.14
        if (IMinecraft.getMinecraftProtocolVersion() < 477 || FrameworkConstants.MAPPING_SYSTEM != FrameworkConstants.MappingSystem.Yarn) {
            logger.error("The Fabric API installer only works on 1.14+");
            Bootstrap.callMethod("EMC-Marketplace", "setStatus(The Fabric API is only for Minecraft 1.14 and above)", "fabric-api Installer", null);
        } else {
            new Thread(() -> {
                try {
                    String emcPath = OSUtils.getMCDir(true) + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + getEMCJsonVersion() + File.separator;
                    logger.debug("EMC path is " + emcPath);
                    // Compatibility...
                    if (new File(emcPath + "optifine.jar").exists()) {
                        logger.warn("Uninstalling incompatible version of OptiFabirc");
                        FileUtils.writeStringToFile(new File(emcPath + "optifine.jar.delete"), "Delete mod", "UTF-8");
                        FileUtils.writeStringToFile(new File(emcPath + "optifabric.jar.delete"), "Delete mod", "UTF-8");
                    }
                    if (!new File(emcPath + "fabric-api.jar").exists()) {
                        JsonObject data = new Gson().fromJson(get(dataURL), JsonObject.class).get(IMinecraft.getMinecraftVersion()).getAsJsonObject();
                        if (data.has("url")) {
                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installing Fabric API, do not close Minecraft...)", "fabric-api Installer", null);
                            IChat.sendClientMessage("Installing Fabric API for " + IMinecraft.getMinecraftVersion());
                            download(data.get("url").getAsString(), emcPath + "fabric-api.jar");
                            logger.info("Installed Fabric API, restart mc");
                            IChat.sendClientMessage("Installed Fabric API, please restart Minecraft");
                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installed Fabric API, please restart mc)", "fabric-api Installer", null);
                        } else {
                            logger.warn("Fabric API not available for " + IMinecraft.getMinecraftVersion());
                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Fabric API is not available for mc " + IMinecraft.getMinecraftVersion() + ")", "fabric-api Installer", null);
                        }
                    }
                } catch (Exception ex) {
                    IChat.sendClientMessage("Error: Failed to install Fabric API, see latest.log");
                    logger.error("Failed to install Fabric API:");
                    Bootstrap.callMethod("EMC-Marketplace", "setStatus(Could not install Fabric API, see latest.log)", "fabric-api Installer", null);
                    ex.printStackTrace();
                }
            }).start();
        }
    }

    @Override
    public void callMethod(String method, String caller, Object object) {
        if (method.equalsIgnoreCase("uninstall()")) {
            logger.info("Uninstalling Fabric API...");
            try {
                String path = OSUtils.getMCDir(true) + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + getEMCJsonVersion() + File.separator;
                FileUtils.writeStringToFile(new File(path + "fabric-api.jar.delete"), "Delete mod", "UTF-8");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void download(String uri, String fileName) throws Exception {
        URL url = new URL(uri);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
        connection.setRequestMethod("GET");
        FileOutputStream out = new FileOutputStream(fileName);
        InputStream in = connection.getInputStream();
        int read;
        byte[] buffer = new byte[4096];
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.close();
    }

    private String get(String url) throws Exception {
        URL url1 = new URL(url);
        Object connection = (url.startsWith("https://") ? (HttpsURLConnection) url1.openConnection()
                : (HttpURLConnection) url1.openConnection());
        ((URLConnection) connection).setConnectTimeout(8 * 1000);
        ((URLConnection) connection).setRequestProperty("User-Agent", "EMC Installer");
        ((HttpURLConnection) connection).setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(((URLConnection) connection).getInputStream()));
        StringBuilder result = new StringBuilder();
        String text;
        while ((text = in.readLine()) != null) {
            result.append(text);
        }
        in.close();
        return result.toString();
    }

    private String getEMCJsonVersion() throws Exception {
        File jsonFile = getEMCJsonFile();
        String version = null;
        if (!jsonFile.exists()) {
            throw new Exception("Could not find EMC json file!");
        } else {
            for (JsonElement jsonElement : lookupElementInJson(jsonFile, "libraries").getAsJsonArray()) {
                JsonObject entry = jsonElement.getAsJsonObject();
                if (entry.get("name").getAsString().contains("me.deftware:EMC-F")) {
                    version = entry.get("name").getAsString().split(":")[2];
                }
            }
        }
        return version;
    }

}
